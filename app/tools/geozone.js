/**
 * Created by vdzundza on 05.01.2015.
 */

/**
 * This function imports all *.geojson files into mongodb storage
 * @param geozonesFilesFolder
 * @param cb - {Function} callback will be invoked when task has been finished
 * (TODO: I planned to use async.js or Promises instead callbacks but now it is not necessary)
 */
function importGeozones(geozonesFilesFolder, cb){
return cb(null);
}


module.exports = {
importGeozones: importGeozones
};