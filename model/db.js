/**
 * Created by Viktor on 1/5/2015.
 */
var mongoose = require('mongoose');
var GeoJSON = require('mongoose-geojson-schema');
var ObjectId = mongoose.Schema.Types.ObjectId;
mongoose.connect('mongodb://test:test@dbh26.mongolab.com:27267/geotest', { db : { safe : false }} );
//console.log('init schemas');
/*
 - name: random
 - value:  1, 2, 3 or 4
 - location: (point:lat/long)
 * */
var userSchema = mongoose.model('User', {
    name:    String,
    value:   Number,
    loc:     {type: [Number], index: '2dsphere'},
    geozone: {type: ObjectId, ref: 'Geozone' }
});

var geozoneSchema = mongoose.model('Geozone', {
    geoFeature:  GeoJSON.Feature,
    //average:    {type: Number, default: null},
    updated_at: {type: Date, default: Date.now},
    users     : [{ type: ObjectId, ref: 'User' }]
});


module.exports = {
    User: userSchema,
    Geozone: geozoneSchema
};
