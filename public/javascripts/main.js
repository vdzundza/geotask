/**
 * Created by Volodymyr Krekhovetskyi on 05.01.15.
 */

var map = new L.Map('map_canvas', {center: new L.LatLng(54.00, -2.00), zoom: 6});
map.addLayer(new L.Google('ROADMAP'));
var geojson;
function getColor(d) {
    return d === 4 ? '#FC4E2A' :
        d >= 3 ? '#FD8D3C' :
            d >= 2 ? '#FEB24C' :
                d >= 1 ? '#FED976' :
                    '#FFEDA0';
}
function styleData(feature) {
    return {
        fillColor: getColor(feature.properties.average),
        weight: 2,
        opacity: 1,
        color: 'white',
        dashArray: '3',
        fillOpacity: 0.7
    };
}
function highlightFeature(e) {
    var layer = e.target;
    layer.setStyle({
        wight: 5,
        color: '#666',
        dashArray: '',
        filOpacity: 0.7
    });

    if (!L.Browser.ie && !L.Browser.opera) {
        layer.bringToFront();
    }
}
function resetHighlight(e) {
    geojson.resetStyle(e.target);
}
function zoomToFeature(e) {
    map.fitBounds(e.target.getBounds());
}
function onEachFeature(feature, layer) {
    layer.on({
        mouseover: highlightFeature,
        mouseout: resetHighlight,
        click: zoomToFeature
    });
}
$.ajax({
    url: '/getData',
    type: "GET",
    dataType: 'json',
    success: function (statesData) {
            geojson = L.geoJson(statesData, {
                style: styleData,
                onEachFeature: onEachFeature
            }).addTo(map);
    }
});

var legend = L.control({position: 'bottomright'});

legend.onAdd = function (map) {

    var div = L.DomUtil.create('div', 'info legend'),
        grades = [1, 2, 3, 4];

    for (var i = 0; i < grades.length; i++) {
        div.innerHTML +=
            '<i style="background:' + getColor(grades[i]) + '"></i> ' +
            grades[i] + (grades[i + 1] ? '&ndash;' + grades[i + 1] + '<br>' : '');
    }

    return div;
};
legend.addTo(map);