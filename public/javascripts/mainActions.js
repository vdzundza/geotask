/**
 * Created by vdzundza on 06.01.2015.
 */
(function ($) {
    $(function () {
        $('#import').click(function () {
            var $button = $(this);
            $button.toggleClass('active');
            $button.prop('disabled', true);
            $.ajax({
                url: '/import',
                method: 'GET',
                success: function (response) {
                    $button.toggleClass('active');
                    $button.removeProp('disabled');
                }
            });
        });

        $("#calculate").on('click', function(){
            $.ajax({
                url: '/calculate',
                method: 'GET',
                dataType: 'json',
                success: function (response) {
                    alert('calculated');
                    console.log(response);
                }
            });
        });
    });
})(jQuery);