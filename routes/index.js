var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var User = require('./../model/db').User;
var Geozone = require('./../model/db').Geozone;
var chance = require('chance').Chance();
var fs = require('fs');
var path = require('path');
var util = require('util');
var async = require('async');
/* GET home page. */

function createUserInsideGeozone(foundCount, maxUsers, cb) {
    if (foundCount >= maxUsers) {
        cb(null, true);
    }
    createFakeUser({min: -4.4, max: 1.7}, {min: 50, max: 54}, function (err, fakeUser) { //options not used
        if (err) throw err;
        Geozone.findOneAndUpdate({
            "geoFeature.geometry": {
                "$geoIntersects": {
                    "$geometry": {
                        "type": "Point",
                        "coordinates": fakeUser.loc
                    }
                }
            }
        }, {$push: {"users": fakeUser}}, function (err, foundGeozone) {
            if (err) throw err;
            if (foundGeozone) {
                fakeUser.geozone = foundGeozone;
                fakeUser.save();
                foundCount++;
            }
            createUserInsideGeozone(foundCount);
        });
    });


}

function createFakeUser(longOption, latOption, cb) {
    var user = new User();
    user.name = chance.name();
    user.value = Math.floor(Math.random() * 4) + 1;
    var long = chance.longitude(longOption);
    var lat = chance.latitude(latOption);
    user.loc = [long.toFixed(2), lat.toFixed(2)];
    return cb(null, user);
}


router.get('/', function (req, res) {
    res.render('index');
});

router.get('/getData', function (req, res) {
    Geozone.find({}, {'_id': 0}, function (err, geozones) {

        var geoFeature = geozones.map(function (v) {
            return v.geoFeature;
        });

        res.json(geoFeature);
    });
});

router.get('/import', function (req, res) {
    //CLEAR DB
    /*
     async.parallel([
     function (cb) {
     return User.remove({}, cb);
     },
     function (cb) {
     Geozone.count({}, function (err, count) {
     //if (count > 0) {
     //    console.log('Geozone are filled.Dont need to clear.  Return');
     //    return cb(null, true); //don't clear geozones for test
     //} else {
     return Geozone.remove({}, cb);
     //}
     });
     }
     ],// optional callback
     function (err, results) {
     console.log('db has been cleaned');
     });
     */
    async.auto({
        fillGeozones: function (callback) {
            Geozone.count({}, function (err, count) {
                if (count > 0) {
                    //console.log("Geozone are filled. Return");
                    return callback(null, true); //don't fill geozones again
                } else {
                    var geoJsonDir = './../geoJSON'; //todo: should be in config
                    var geoJsonFullDir = path.join(__dirname, geoJsonDir);
                    var savedCount = 0, countGeozones = -1;
                    fs.readdir(geoJsonFullDir, function (err, files) {
                        if (err) throw err;
                        countGeozones = files.length;
                        files.forEach(function (file) {
                            fs.readFile(path.join(geoJsonFullDir, file), 'utf-8', function (err, geoJson) {
                                var geoZone = new Geozone();
                                geoZone.geoFeature = JSON.parse(geoJson);
                                geoZone.geoFeature.properties.average = null;
                                geoZone.save(function (err, savedGeozone) {
                                    if (err) throw err;
                                    savedCount++;
                                });
                            });
                        });
                    });

                    var interval = setInterval(function () {
                        if (savedCount === countGeozones) {
                            clearInterval(interval);
                            return callback(null, true);
                        }
                    }, 1000);
                }
            });
        },
        fill_users: ['fillGeozones', function (callback, results) {
            return createUserInsideGeozone(0, 20000, callback);
        }]
    }, function (err, results) {
        res.json({status: 0});
    });
});

router.get('/calculate', function (req, res) {
    var average = 0;
    var db = mongoose.connection;
    db.model('User').aggregate(
        [
            {$group: {_id: "$geozone", avg: {$avg: "$value"}}}
        ]
        ,
        function (err, results) {
            if (err) {
                return res.json({status: 1, message: "Error during calculating average value"});
            }
            results.forEach(function (item, index, arr) {
                Geozone.findOneAndUpdate({"_id": item._id}, {$set: {"geoFeature.properties.average": item.avg}},
                    function (err, updated) {
                        if (err) {
                            return res.json({status: 1, message: "Error during updating geozone average value"});
                        }
                    });
            });
            return res.json({status: 0});
        });
});

module.exports = router;
